<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="/css/bootstrap.css"/>
    <link href="/css/bootstrap.style.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css"/>

    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/libs/json2html.js"></script>
    <script type="text/javascript" src="/js/libs/jquery.json2html.js"></script>
    <script>
        $('#uploadForm', document).submit(function (event) {
            event.preventDefault();

            var formData = new FormData($(this)[0]);

            $.ajax({
                url: 'uploadBook',
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (returnData) {
                    alert(returnData);
                }
            });

            return false;
        });
        $(document).ready(function () {
            loadEntitiesToSelect('#publisher', 'entityList', "PUBLISHER");
            loadEntitiesToSelect('#seria', 'entityList', "SERIA");
            loadEntitiesToSelect('#keywords', 'entityList', "KEYWORD");
            loadEntitiesToSelect('#authors', 'entityList', "AUTHOR");
        });

        function loadEntitiesToSelect(identifier, url, type) {
            console.log(type);
            $.ajax({
                url: url,
                data: {entityType: type},
                success: function (entities) {
                    entities.forEach(function (entity) {
                        if (type !== 'AUTHOR') {
                            $(identifier).append($('<option></option>').attr('value', entity['id']).text(entity['name']))
                        } else {
                            console.log(entity);
                            $(identifier).append($('<option></option>').attr('value', entity['id'])
                                    .text(entity['firstName'] + ' ' + entity['secondName']))
                        }
                    });
                }
            });
        }
    </script>
</head>
<body>
<%@include file="header.jsp" %>
<div class="jumbotron">
    <div class="container">
        <form method="POST" action="/library/uploadBook" class="form-horizontal" id="uploadForm"
              enctype="multipart/form-data">
            <div class="form-group">
                <label for="title" class="control-label col-xs-2"><fmt:message key="library.form.title"
                                                                               bundle="${lang}"/></label>

                <div class="col-xs-10">
                    <input type="text" class="form-control" id="title" name="title"
                           placeholder="<fmt:message key="library.form.title" bundle="${lang}"/>" required>
                </div>
            </div>

            <div class="form-group">
                <label for="year" class="control-label col-xs-2"><fmt:message key="library.form.year"
                                                                              bundle="${lang}"/></label>

                <div class="col-xs-10">
                    <input type="text" class="form-control" id="year" name="year"
                           placeholder="<fmt:message key="library.form.year" bundle="${lang}"/>" required>
                </div>
            </div>

            <div class="form-group">
                <label for="size" class="control-label col-xs-2"><fmt:message key="library.form.size"
                                                                              bundle="${lang}"/></label>

                <div class="col-xs-10">
                    <input type="text" class="form-control" id="size" name="size"
                           placeholder="<fmt:message key="library.form.size" bundle="${lang}"/>" required>
                </div>
            </div>

            <div class="form-group">
                <label for="edition" class="control-label col-xs-2"><fmt:message key="library.form.edition"
                                                                                 bundle="${lang}"/></label>

                <div class="col-xs-10">
                    <input type="text" class="form-control" id="edition" name="edition"
                           placeholder="<fmt:message key="library.form.edition" bundle="${lang}"/>" required>
                </div>
            </div>

            <div class="form-group">
                <label for="publisher" class="control-label col-xs-2"><fmt:message key="library.form.publisher"
                                                                                   bundle="${lang}"/></label>

                <div class="col-sm-5">
                    <select class="form-control" id="publisher" required="required" name="publisher">
                    </select>
                </div>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="text" class="form-control" id="newPublisher"
                               placeholder="<fmt:message key="library.form.addPublisher" bundle="${lang}"/>">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" id="addPublisher" type="button"><fmt:message
                                key="library.form.add" bundle="${lang}"/></button>
                    </span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="seria" class="control-label col-xs-2"><fmt:message key="library.form.seria"
                                                                               bundle="${lang}"/></label>

                <div class="col-sm-5">
                    <select class="form-control" id="seria" name="seria">
                        <option value=""></option>
                    </select>
                </div>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="text" class="form-control" id="newSeria"
                               placeholder="<fmt:message key="library.form.addSeria" bundle="${lang}"/>">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" id="addSeria" type="button"><fmt:message key="library.form.add"
                                                                                                 bundle="${lang}"/></button>
                    </span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="keywords" class="control-label col-xs-2"><fmt:message key="library.form.keywords"
                                                                                  bundle="${lang}"/></label>

                <div class="col-sm-5">
                    <select multiple class="form-control" id="keywords" name="keywords" required>
                        <option value=""></option>
                    </select>
                </div>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="text" class="form-control" id="newKeyword"
                               placeholder="<fmt:message key="library.form.addKeyword" bundle="${lang}"/>">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" id="addKeyword" type="button"><fmt:message
                                key="library.form.add" bundle="${lang}"/></button>
                    </span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="authors" class="control-label col-xs-2"><fmt:message key="library.form.authors"
                                                                                 bundle="${lang}"/></label>

                <div class="col-sm-5">
                    <select multiple class="form-control" id="authors" name="authors" required>
                        <option value=""></option>
                    </select>
                </div>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="text" class="form-control" id="newAuthor"
                               placeholder="<fmt:message key="library.form.addAuthor" bundle="${lang}"/>">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" id="addAuthor" type="button"><fmt:message key="library.form.add"
                                                                                                  bundle="${lang}"/></button>
                    </span>
                    </div>
                </div>
                <div class="form-group col-md-5 input-file-button">
                    <label for="file" class="control-label col-xs-2 upload-file-label"><fmt:message
                            key="library.form.file"
                            bundle="${lang}"/></label>
                    <span class="btn btn-primary btn-file pull-right" type="file" class="form-control" id="file"
                          name="file"
                          placeholder="<fmt:message key="library.form.file" bundle="${lang}"/>">
                       <span class="upload-icon glyphicon glyphicon-upload"></span> <span class="input-file-name">Завантажити файл</span><input
                            type="file" id="upload-book" required>
                    </span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-offset-2 col-xs-10">
                    <button type="submit" class="btn btn-primary"><fmt:message key="library.form.addBook"
                                                                               bundle="${lang}"/></button>
                </div>
            </div>
        </form>
    </div>
</div>
<%@include file="footer.jsp" %>
</body>
</html>
