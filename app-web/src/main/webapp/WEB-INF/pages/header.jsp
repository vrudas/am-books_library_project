<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:set value="ukrainian" var="language" />
<c:if test="${not empty sessionScope.LANGUAGE}" >
    <c:set value="${sessionScope.LANGUAGE}" var="language" />
</c:if>

<fmt:setBundle basename="${language}" var="lang"/>

<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.js"></script>
<script src="/js/main.js"></script>
<script src="/js/jquery.validate.min.js"></script>

<script>
    $(document).ready(function () {
        $('.language_js').on('click', function () {
            var selectedLang = $(this).data("language");

            $.ajax({
                url: '/changeLanguage',
                type: 'POST',
                data: {language: selectedLang}
            });
        });
    });
</script>

<div class="navbar navbar-default ">
    <div class="navbar-header head-menu">
        <img src="/img/logo.png" height="60" alt=""/>
    </div>
    <button type="button" class="navbar-toggle collapsed toogle-navbar-button" data-toggle="collapse" data-target="#navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <div class="collapse navbar-collapse navbar-right" id="navbar-collapse">
        <ul class="nav navbar-nav">
            <li><a href="/"><fmt:message key="library.menu.main" bundle="${lang}"/></a></li>
            <li><a href="/library/bookSearch"><fmt:message key="library.menu.bookSearch" bundle="${lang}"/></a></li>
            <li><a href="/library/bookList"><fmt:message key="library.menu.bookList" bundle="${lang}"/></a></li>
            <sec:authorize access="isAuthenticated()">
            <li><a href="/library/addBook"><fmt:message key="library.menu.addBook" bundle="${lang}"/></a></li>
            </sec:authorize>
            <li><a href="#" data-toggle="modal" data-target="#loginModal"><fmt:message key="library.menu.login" bundle="${lang}"/></a></li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">
                    <fmt:message key="library.menu.internationalization" bundle="${lang}"/> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="language_js" data-language="ukrainian"><fmt:message key="library.menu.internationalization.ukrainian" bundle="${lang}"/></a></li>
                    <li><a href="#" class="language_js" data-language="english"><fmt:message key="library.menu.internationalization.english" bundle="${lang}"/></a></li>
                    <li><a href="#" class="language_js" data-language="ukrainian"><fmt:message key="library.menu.internationalization.russian" bundle="${lang}"/></a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>

<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><fmt:message key="library.login.singIn" bundle="${lang}"/></h4>
            </div>
            <div class="modal-body">
                <c:url value="/j_spring_security_check" var="loginUrl"/>
                <form action="${loginUrl}" method="post">
                    <input type="text" class="form-control" name="j_username" placeholder="<fmt:message key="library.login.emailAddress" bundle="${lang}"/>" required
                           autofocus>
                    <br/>
                    <input type="password" class="form-control" name="j_password" placeholder="<fmt:message key="library.login.password" bundle="${lang}"/>" required>
                    <br/>
                    <button class="btn btn-bg btn-primary btn-block" type="submit"><fmt:message key="library.login.login" bundle="${lang}"/></button>
                </form>
            </div>
        </div>
    </div>
</div>


