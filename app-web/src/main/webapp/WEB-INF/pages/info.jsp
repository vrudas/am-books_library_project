<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <title></title>
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="/css/bootstrap.css"/>
  <link href="/css/bootstrap.style.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/css/style.css"/>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  <script></script>
</head>
<body>
<%@include file="header.jsp" %>
<div class="jumbotron">
  <div class="container">
    ${message}
  </div>
</div>
<%@include file="footer.jsp" %>
</body>
</html>
