<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title></title>
</head>
<body>
<c:forEach items="${list}" var="book">
    <%--${book.id}--%>
    ${book.title}
    ${book.year}
    ${book.size}
    ${book.edition}
    ${book.publisher.name}
    ${book.seria.name}
    ${book.extension}
    <c:if test="${not empty book.authors}">
        ${book.authors}
    </c:if>
    <c:if test="${not empty book.keywords}">
        ${book.keywords}
    </c:if>
    ${book.bookFile.file}
    <br>
</c:forEach>
</body>
</html>
