<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <title></title>
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="/css/bootstrap.css"/>
  <link href="/css/bootstrap.style.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/css/style.css"/>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  <script></script>
</head>

<body>
<%@include file="header.jsp" %>
<div class="jumbotron">
  <div class="container">
    <table class="table table-striped book-list">
      <thead>
      <tr>
        <th><span class="glyphicon glyphicon-align-justify"></span></th>
        <th><fmt:message key="library.books.name" bundle="${lang}"/></th>
        <th><fmt:message key="library.books.year" bundle="${lang}"/></th>
        <th><fmt:message key="library.books.size" bundle="${lang}"/></th>
        <th><fmt:message key="library.books.edition" bundle="${lang}"/></th>
        <th><fmt:message key="library.books.publisher" bundle="${lang}"/></th>
        <th><fmt:message key="library.books.seria" bundle="${lang}"/></th>
        <th><fmt:message key="library.books.author" bundle="${lang}"/></th>
        <th><fmt:message key="library.books.genre" bundle="${lang}"/></th>
        <th><fmt:message key="library.books.extansion" bundle="${lang}"/></th>
        <th><fmt:message key="library.books.download" bundle="${lang}"/></th>
      </tr>
      </thead>
      <tbody>
      <c:forEach items="${list}" var="book" varStatus="index">
        <tr>
          <td>${index.index + 1}</td>
          <td>${book.title}</td>
          <td>${book.year}</td>
          <td>${book.size}</td>
          <td>${book.edition}</td>
          <td>${book.publisher.name}</td>
          <td>${book.seria.name}</td>
          <td>
            <c:if test="${not empty book.authors}">
              <c:forEach items="${book.authors}" var="author">
                ${author.firstName} ${author.secondName}
              </c:forEach>
            </c:if>
          </td>
          <td>
            <c:if test="${not empty book.keywords}">
              <c:forEach items="${book.keywords}" var="keyword" varStatus="i">
                ${keyword.name};
              </c:forEach>
            </c:if>
          </td>
          <td>${book.extension}</td>
          <td><a href="downloadBook/${book.id}" class="btn btn-primary btn-sm glyphicon glyphicon-floppy-save download-button"></a></td>
        </tr>
      </c:forEach>
      </tbody>
    </table>
    <%--<sec:authorize access="!isAuthenticated()">--%>
    <%--<p><a class="btn btn-lg btn-success" href="<c:url value="/login" />" role="button">Login</a></p>--%>
    <%--</sec:authorize>--%>
    <%--<sec:authorize access="isAuthenticated()">--%>
    <%--<p>Ваш логин: <sec:authentication property="principal.username"/></p>--%>
    <%--<p><a class="btn btn-lg btn-danger" href="<c:url value="/logout" />" role="button">Logout</a></p>--%>
    <%--</sec:authorize>--%>
  </div>
</div>
<%@include file="footer.jsp" %>
</body>
</html>
