<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.css"/>
    <link href="css/bootstrap.style.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css"/>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>-->
    <!--<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>-->
    <%--<![endif]-->--%>
</head>

<body>
<%@include file="header.jsp" %>
<div class="jumbotron">
    <div class="container">
        <div class="invite-text">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
            Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis,
            ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo,
            fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
            justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper
            nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.
            Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius
            laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies
            nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero,
            sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
            Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis
            ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec
            sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,
        </div>
        <hr/>
        <h3><fmt:message key="library.main.lastAdded" bundle="${lang}"/></h3>
        <table class="table table-striped book-list">
            <thead>
            <tr>
                <th><span class="glyphicon glyphicon-align-justify"></span></th>
                <th><fmt:message key="library.main.name" bundle="${lang}"/></th>
                <th><fmt:message key="library.main.author" bundle="${lang}"/></th>
                <th><fmt:message key="library.main.genre" bundle="${lang}"/></th>
                <th><fmt:message key="library.main.download" bundle="${lang}"/></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Column content</td>
                <td>Column content</td>
                <td>Column content</td>
                <td><a href="#" class="btn btn-primary btn-sm glyphicon glyphicon-floppy-save download-button"></a></td>
            </tr>
            <tr>
                <td>1</td>
                <td>Column content</td>
                <td>Column content</td>
                <td>Column content</td>
                <td><a href="#" class="btn btn-primary btn-sm glyphicon glyphicon-floppy-save download-button"></a></td>
            </tr>
            <tr>
                <td>1</td>
                <td>Column content</td>
                <td>Column content</td>
                <td>Column content</td>
                <td><a href="#" class="btn btn-primary btn-sm glyphicon glyphicon-floppy-save download-button"></a></td>
            </tr>
            </tbody>
        </table>
        <%--<sec:authorize access="!isAuthenticated()">--%>
        <%--<p><a class="btn btn-lg btn-success" href="<c:url value="/login" />" role="button">Login</a></p>--%>
        <%--</sec:authorize>--%>
        <%--<sec:authorize access="isAuthenticated()">--%>
        <%--<p>Ваш логин: <sec:authentication property="principal.username"/></p>--%>
        <%--<p><a class="btn btn-lg btn-danger" href="<c:url value="/logout" />" role="button">Logout</a></p>--%>
        <%--</sec:authorize>--%>
    </div>
</div>
<%@include file="footer.jsp" %>
</body>
</html>
