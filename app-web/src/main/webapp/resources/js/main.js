$(function () {
    /*Internationalization reload*/
    $(".dropdown-menu li").click(function () {
        window.location.reload();
    });

    $("#upload-book").on('change',function(){
       $(".input-file-name").text($(this).val().substr($(this).val().lastIndexOf('\\') + 1));
    });

    $("#uploadForm").validate();
});