package com.blp.web.utils;

import java.util.Collection;

public class StringUtil {

    public static String colletionToString(Collection collection){
        String result = "";
        for(Object o:collection){
            result += " " + o.toString();
        }

        return result;
    }

    public static String firstToUpper(String s){
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }
}
