package com.blp.web.utils;

import com.blp.web.library.entities.Keyword;

import java.util.HashSet;
import java.util.Set;

public class Util {
    private static void keywords(Set<Keyword> keywords, Keyword keyword) {

        if(keyword.getParent() == null) {
            keywords.add(keyword);
        } else {
            keywords(keywords, keyword.getParent());
            keywords.add(keyword);
        }
    }

    public static Set<Keyword> getKeys(Keyword keyword){
        Set<Keyword> keywords = new HashSet<>();
        keywords(keywords, keyword);

        return keywords;
    }
}
