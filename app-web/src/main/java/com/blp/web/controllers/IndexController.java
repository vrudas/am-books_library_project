package com.blp.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/")
public class IndexController {
    @RequestMapping(value = "")
    public String clear() {
        return "index";
    }

    @RequestMapping(value = "index")
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/changeLanguage")
    @ResponseBody
    public String changeLanguage(HttpServletRequest request, String language) {
        request.getSession().setAttribute("LANGUAGE", language);
        return language;
    }
}
