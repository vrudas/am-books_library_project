package com.blp.web.test;

import com.blp.web.library.entities.Book;
import com.blp.web.library.entities.Publisher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TestService {
    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public void createEntity(TestEntity entity){
//        getCurrentSession().save(entity);
        List<Book> book = (List<Book>) getCurrentSession().createQuery("from Book order by title").list();
//        book.forEach(bk -> System.out.println(bk.getBookFile().getFileName()));

    }
}
