package com.blp.web.auth;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao {
    @Autowired
    private SessionFactory sessionFactory;

    public User getUserByLogin(String login) {
        return (User) sessionFactory.getCurrentSession()
                .createQuery("from User where login = ?")
                .setParameter(0, login)
                .uniqueResult();
    }
}
