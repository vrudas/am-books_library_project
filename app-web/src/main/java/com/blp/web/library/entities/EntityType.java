package com.blp.web.library.entities;

public enum EntityType {
    PUBLISHER(Publisher.class),
    KEYWORD(Keyword.class),
    SERIA(Seria.class),
    AUTHOR(Author.class);

    private final Class entityClass;

    EntityType(Class entityClass) {
        this.entityClass = entityClass;
    }

    public Class getEntityClass() {
        return entityClass;
    }
}
