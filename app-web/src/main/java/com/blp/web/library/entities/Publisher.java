package com.blp.web.library.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "publisher")
public class Publisher {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "publisher")
    private Set<Book> publisherBooks;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Book> getPublisherBooks() {
        return publisherBooks;
    }

    public void setPublisherBooks(Set<Book> publisherBooks) {
        this.publisherBooks = publisherBooks;
    }
}
