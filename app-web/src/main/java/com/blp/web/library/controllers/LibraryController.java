package com.blp.web.library.controllers;

import com.blp.web.library.entities.Book;
import com.blp.web.library.entities.BookFile;
import com.blp.web.library.forms.BookUploadForm;
import com.blp.web.library.services.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@Controller
@RequestMapping(value = "/library")
public class LibraryController {

    @Autowired
    LibraryService libraryService;

    @RequestMapping(value = "/addBook")
    public String addBook() {
        return "addBook";
    }

    @RequestMapping(value = "/uploadBook")
    public String uploadBook(Model model, BookUploadForm form) {
        String message;

        try {
            Book book = libraryService.prepareBook(form);
            libraryService.addBook(book);

            libraryService.saveAuthorsBooks(book, form.getAuthors());

            BookFile bookFile = new BookFile(book.getId(), form.getFile().getBytes());
            libraryService.addBookFile(bookFile);

            book.setBookFile(bookFile);
            libraryService.updateBook(book);

            message = "Book " + book.getTitle() + " added!";
        } catch (Exception e) {
            message = "Uploading error!";
            e.printStackTrace();
        }

        model.addAttribute("message", message);

        return "info";
    }

    @ResponseBody
    @RequestMapping(value = "/entityList")
    public List getEntities(String entityType) {
        return libraryService.getEntitiesList(entityType);
    }

    @RequestMapping(value = "/bookList")
    public String getBooks(Model model) {
        List bookList = libraryService.getBookList();
        System.out.println(bookList);
        model.addAttribute("list", bookList);

        return "bookList";
    }

    @RequestMapping(value = "/downloadBook/{bookId}")
    public void downloadBook(@PathVariable Integer bookId, HttpServletResponse response) {
        Book book = libraryService.getBookById(bookId);
        BookFile bookFile = libraryService.downloadBookFile(bookId);
        try (OutputStream outputStream = response.getOutputStream()) {
            response.setHeader("Content-Disposition", "inline;filename=" + book.getTitle() + "." + book.getExtension());
            response.setContentType("application/force-download");
            outputStream.write(bookFile.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
