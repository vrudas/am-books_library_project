package com.blp.web.library.forms;

import org.springframework.web.multipart.MultipartFile;

import java.util.Set;

public class BookUploadForm {
    private String title;
    private Integer year;
    private Integer size;
    private String edition;
    private Integer publisher;
    private Integer seria;
    private Set<Integer> keywords;
    private Set<Integer> authors;
    private MultipartFile file;

    @Override
    public String toString() {
        return "UploadBookForm{" +
                "title='" + title + '\'' +
                ", year=" + year +
                ", size=" + size +
                ", edition='" + edition + '\'' +
                ", publisher=" + publisher +
                ", seria=" + seria +
                ", keywords=" + keywords +
                ", authors=" + authors +
                ", file=" + file +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public Integer getPublisher() {
        return publisher;
    }

    public void setPublisher(Integer publisher) {
        this.publisher = publisher;
    }

    public Integer getSeria() {
        return seria;
    }

    public void setSeria(Integer seria) {
        this.seria = seria;
    }

    public Set<Integer> getKeywords() {
        return keywords;
    }

    public void setKeywords(Set<Integer> keywords) {
        this.keywords = keywords;
    }

    public Set<Integer> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Integer> authors) {
        this.authors = authors;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

}
