package com.blp.web.library.entities;

import javax.persistence.*;

@Entity
@Table(name = "file")
public class BookFile {

    @Id
    @GeneratedValue
    private Integer id;

    private Integer bookId;

    @Lob
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "bookFile", cascade = CascadeType.ALL)
    private Book book;

    @Column(name = "bookFile")
    private byte[] file;

    public BookFile(){

    }

    public BookFile(Integer bookId, byte[] file) {
        this.bookId = bookId;
        this.file = file;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }


}
