package com.blp.web.library.services;

import com.blp.web.library.entities.*;
import com.blp.web.library.forms.BookUploadForm;
import org.apache.commons.io.FilenameUtils;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class LibraryService {

    @Autowired
    SessionFactory sessionFactory;

    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public void addBook(Book book) {
        getCurrentSession().save(book);
    }

    public Book prepareBook(BookUploadForm bookForm) throws IOException {
        Book book = new Book();

        Publisher publisher = null;
        if (bookForm.getPublisher() != null) {
            publisher = (Publisher) getUniqueObject(Publisher.class, bookForm.getPublisher());
        }

        Seria seria = null;
        if (bookForm.getSeria() != null) {
            seria = (Seria) getUniqueObject(Seria.class, bookForm.getSeria());
        }

        Set keywords = null;
        if (bookForm.getKeywords() != null) {
            keywords = getSetOfObjects(Keyword.class, bookForm.getKeywords());
        }

        book.setTitle(bookForm.getTitle());
        book.setYear(bookForm.getYear());
        book.setSize(bookForm.getSize());
        book.setEdition(bookForm.getEdition());
        book.setPublisher(publisher);
        book.setSeria(seria);
        book.setKeywords(keywords);
        book.setExtension(FilenameUtils.getExtension(bookForm.getFile().getOriginalFilename()));

        return book;
    }

    public List getEntitiesList(String entityType) {
        List entities;

        Class entityClass = EntityType.valueOf(entityType).getEntityClass();
        entities = getCurrentSession().createCriteria(entityClass).list();

        return entities;
    }

    public List getBookList() {
        List<Book> list = getCurrentSession().createCriteria(Book.class).list();

        list.stream().forEach(book -> {
            Hibernate.initialize(book.getPublisher());
            Hibernate.initialize(book.getSeria());
            Hibernate.initialize(book.getAuthors());
            Hibernate.initialize(book.getKeywords());
        });

        return list;
    }

    private Object getUniqueObject(Class criteriaClass, Integer entityId) {
        return getCurrentSession().createCriteria(criteriaClass).add(Restrictions.eq("id", entityId)).uniqueResult();
    }

    private Set getSetOfObjects(Class entityClass, Collection ids) {
        String entityName = entityClass.getSimpleName();
        String queryString = "from " + entityName + " e where e.id  in (:entitiesId)";
        List entities = getCurrentSession().createQuery(queryString).setParameterList("entitiesId", ids).list();

        return new HashSet<>(entities);
    }

    public void updateBook(Book book) {
        getCurrentSession().update(book);
    }

    public void addBookFile(BookFile bookFile) {
        getCurrentSession().save(bookFile);
    }

    public void saveAuthorsBooks(Book book, Set<Integer> authorsId) {
        Set<Author> authors = getSetOfObjects(Author.class, authorsId);
        authors.stream().forEach(author -> {
            author.getBooks().add(book);
            getCurrentSession().update(author);
        });
    }

    public BookFile downloadBookFile(Integer bookId) {
        BookFile bookFile = (BookFile) getCurrentSession().createCriteria(BookFile.class).add(Restrictions.eq("bookId", bookId)).uniqueResult();
        Hibernate.initialize(bookFile.getFile());

        return bookFile;
    }

    public Book getBookById(Integer id) {
        return (Book) getUniqueObject(Book.class, id);
    }
}
