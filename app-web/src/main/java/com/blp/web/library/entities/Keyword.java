package com.blp.web.library.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "keyword")
public class Keyword implements Comparable<Keyword> {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "parentId")
    private Keyword parent;

    @ManyToMany(mappedBy = "parent")
    private Set<Keyword> children = new HashSet<>();

    @ManyToMany(mappedBy = "keywords")
    private Set<Book> books = new HashSet<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Keyword getParent() {
        return parent;
    }

    public void setParent(Keyword parent) {
        this.parent = parent;
    }

    public Set<Keyword> getChildren() {
        return children;
    }

    public void setChildren(Set<Keyword> children) {
        this.children = children;
    }

    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }

    @Override
    public int compareTo(Keyword another) {
        if (another.id < this.id) {
            return -1;
        }
        if (another.id > this.id) {
            return 1;
        }
        return 0;
    }
}
