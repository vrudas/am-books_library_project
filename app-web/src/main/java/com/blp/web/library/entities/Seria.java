package com.blp.web.library.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "seria")
public class Seria {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "seria")
    private Set<Book> seriaBooks;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Book> getSeriaBooks() {
        return seriaBooks;
    }

    public void setSeriaBooks(Set<Book> seriaBooks) {
        this.seriaBooks = seriaBooks;
    }
}
